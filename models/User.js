const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({

    fullName: {
        type: String,
        required: [true, "Full Name is required."]
    },
    email: {
        type: String,
        required: [true, "Email is required."]
    },
    password: {
        type: String,
        required: [true, "Password is required."]
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    orders: [
        {
            orderId: {
                type: String,
                required: [true, "Order Id is required."]
            },
            purchasedOn: {
                type: Date,
                default: new Date()
            },
            status: {
                type: String,
                default: "Order Created"
            }
        }
    ]
})

module.exports = mongoose.model("User", userSchema);