const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({

    owner: { 
        type: String,
        required: [true, "User Id is required."]    
    },
    orderProduct: [
        {
            productId: {
                type: String,
                required: [true, "Product Id is required."]
            },
            price: {
                type: Number,
                required: [true, "Price is required."]
            },
            quantity: {
                type: Number,
                min: 1,
                required: [true, "Quantity is required."]
            },
            subtotal: {
                type: Number,
                required: [true, "Subtotal is required."]
            }
        }
    ],
    totalAmount: {
        type: Number,
        required: [true, "Total Amount is required."]
    },
    purchasedOn: {
        type: Date,
        default: new Date()
    },
    status: {
        type: String,
        default: "Order Created"
    }
    
})

module.exports = mongoose.model("Order", orderSchema);