/* ===================== REQUIRED MODULES ===================== */

const mongoose = require("mongoose");
const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");

/* ============================================================ */


/* ============= Controller for creating orders ============ */

module.exports.createOrder = async (userId, reqBody) => {

    console.log(userId)
    console.log(reqBody)

    let orderProduct = reqBody.order

    let productIdCheck = [];

    for (let c = 0; c < orderProduct.length; c++) {

        if(!mongoose.Types.ObjectId.isValid(orderProduct[c].productId)) {
        
            return "Please provide correct Product Id."
    
        } else {

            await Product.findById(orderProduct[c].productId).then(result => {

                if(result == null) {
                    return false
                } else {
                    productIdCheck.push(1)
                }
    
            })
        }

    }

    console.log(productIdCheck)

    if(productIdCheck.length == orderProduct.length) {

        for (let i = 0; i < orderProduct.length; i++) {

            let productPrice = await Product.findById(orderProduct[i].productId).then(result => { return result.price });
    
            orderProduct[i].price = productPrice,
            orderProduct[i].subtotal = (orderProduct[i].price*orderProduct[i].quantity)
    
        }
    
        let total = 0;
    
        for (let j = 0; j < orderProduct.length; j++) {
            total += orderProduct[j].subtotal
        }
    
        let newOrder = new Order ({
    
            owner: userId,
            orderProduct: orderProduct,
            totalAmount: total 
            
        })
    
        let isOrderSaved = await newOrder.save().then((order, error) => {
    
            if(error) {
                return false 
    
            } else {
                return true
            }
        })
    
        let orderId = newOrder._id.toString()

        console.log(orderId)
    
        let isUserUpdated = await User.findById(userId).then(user => {
    
            user.orders.push({orderId: orderId, purchasedOn: newOrder.purchasedOn, status: newOrder.status});
            
            return user.save().then((user,error) => {
    
                if(error){
                    return false
                } else {
                    return true
                }
            })
        })
    
        let productUpdated = [];
    
        for (let p = 0; p < orderProduct.length; p++) {
    
            await Product.findById(orderProduct[p].productId).then(product => {
    
                product.orderedIn.push({orderId: orderId, purchasedOn: newOrder.purchasedOn});
    
                return product.save().then((product,error) => {
    
                    if(error){
                        return false
                    } else {
                        productUpdated.push(1)
                        return true
                    }
                })
            })
    
        }
    
        let isProductUpdated = productUpdated.length == orderProduct.length;
    
        if(isOrderSaved && isUserUpdated && isProductUpdated) {

            return `Order is successfully created. Order Id: ${orderId}`

        } else {

            return "Error encountered while creating order."

        }

    } else {

        return "Please provide correct Product Id."

    }

}

/* ============================================================ */


/* =========== Controller for Retrieving ALL orders =========== */

module.exports.getAllOrders = () => {

    return Order.find({}).then(result => {

        if(result.length > 0) {

            return result

        } else {

            return "No orders at the moment."
        }
    })
}

/* ============================================================ */


/* == Controller for Retrieving Authenticated User's orders === */

module.exports.getMyOrders = (userId) => {

    return Order.find({owner: userId}).then(result => {

        if(result.length > 0) {

            return result

        } else {

            return "No orders at the moment."
        }
    })
}

/* ============================================================ */
